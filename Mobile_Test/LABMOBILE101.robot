*** Settings ***
Library           AppiumLibrary

*** Test Cases ***
Run
    Open Apptest

*** Keywords ***
Open Apptest
    Set Library Search Order    AppiumLibrary
    Open Application    http://127.0.0.1:4723/wd/hub    platformName=Android    platformVersion=11.0    deviceName=emulator-5554    appPackage=com.google.android.youtube    appActivity=com.google.android.apps.youtube.app.application.Shell$ResultsActivity    noReset=true
    Wait Until Page Contains Element    //android.widget.ImageView[@content-desc="Search"]
    Click Element    //android.widget.ImageView[@content-desc="Search"]
    sleep    2s
    Input Text    com.google.android.youtube:id/search_edit_text    Cat
    sleep    2s
    Press Keycode    66
    sleep    3s
    FOR    ${index}    IN RANGE    0    10
        ${aleart}    Run Keyword And Return Status    Page Should Contain Element    //android.view.ViewGroup[contains(@content-desc,"Cats Galore | Badanamu Compilation l Nursery Rhymes")]
        Run Keyword if    '${aleart}'!='True'    Swipe By Percent    50    80    50    10
    END
    Click Element    //android.view.ViewGroup[contains(@content-desc,"Cats Galore | Badanamu Compilation l Nursery Rhymes")]
    Sleep    10s
    Press Keycode    85
    Sleep    1s
    Close All Applications
