*** Settings ***
Library           SeleniumLibrary
Library           DateTime
Library           String
Library           Collections
Library           OperatingSystem
Library           BuiltIn
Library           Dialogs
Library           RequestsLibrary
Library           JSONLibrary
Library           XML
Library           ExcelRobot
Library           Dialogs
Library           AppiumLibrary

*** Test Cases ***
Traval
    Set Library Search Order    ExcelRobot
    Open Excel    D:\\RobotTraining\\Mobile_Test\\Travel.xlsx
    ${columnCountInput}    Get Column Count    Travel
    ${rowCountInput}    Get Row Count    Travel
    ${columnCountOutput}    Get Column Count    OUTPUT
    @{startfrom}    Create List
    @{tosomewhere}    Create List
    @{HourOutpuut}    Create List
    @{KilometerOutput}    Create List
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${TAGS}    Read Cell Data    Travel    0    ${input}
        Run Keyword if    '${TAGS}' != 'RUN'    Continue For Loop
        ${FROM}    Read Cell Data    Travel    1    ${input}
        ${TO}    Read Cell Data    Travel    2    ${input}
        Append To List    ${startfrom}    ${FROM}
        Append To List    ${tosomewhere}    ${TO}
    END
    ${LengthFromTo}    Get Length    ${startfrom}
    FOR    ${j}    IN RANGE    0    ${LengthFromTo}
        ${inputFrom}    Set Variable    ${startfrom}[${j}]
        ${inputTo}    Set Variable    ${tosomewhere}[${j}]
        Open Map
        Search    ${inputFrom}    ${inputTo}    ${HourOutpuut}    ${KilometerOutput}
    END
    Open Excel To Write    D:\\RobotTraining\\Mobile_Test\\Travel.xlsx    D:\\RobotTraining\\Mobile_Test\\TravelCopy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${LengthFromTo}
        ${i}    Evaluate    ${output}+2
        Write To Cell By Name    OUTPUT    A${i}    ${startfrom}[${output}]
        Write To Cell By Name    OUTPUT    B${i}    ${tosomewhere}[${output}]
        Write To Cell By Name    OUTPUT    C${i}    ${HourOutpuut}[${output}]
        Write To Cell By Name    OUTPUT    D${i}    ${KilometerOutput}[${output}]
    END
    Save Excel

Extosoft Website
    Set Library Search Order    ExcelRobot
    Open Excel    D:\\RobotTraining\\Mobile_Test\\DetailsExtosoft.xlsx
    ${columnCountInput}    Get Column Count    INPUT
    ${rowCountInput}    Get Row Count    INPUT
    ${columnCountOutput}    Get Column Count    OUTPUT
    @{word}    Create List
    @{Company}    Create List
    @{Address}    Create List
    @{Telephone}    Create List
    @{Email}    Create List
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${TAGS}    Read Cell Data    INPUT    0    ${input}
        Run Keyword if    '${TAGS}' != 'RUN'    Continue For Loop
        ${Search}    Read Cell Data    INPUT    1    ${input}
        Append To List    ${word}    ${Search}
    END
    Open Chrome
    Search Word    ${word}    ${Company}    ${Address}    ${Telephone}    ${Email}
    Open Excel To Write    D:\\RobotTraining\\Mobile_Test\\DetailsExtosoft.xlsx    D:\\RobotTraining\\Mobile_Test\\DetailsExtosoftCopy.xlsx    override=override
    Write To Cell By Name    OUTPUT    A${2}    ${Company}[0]
    Write To Cell By Name    OUTPUT    B${2}    ${Address}[0]
    Write To Cell By Name    OUTPUT    C${2}    ${Telephone}[0]
    Write To Cell By Name    OUTPUT    D${2}    ${Email}[0]
    Save Excel

*** Keywords ***
Open Map
    Set Library Search Order    AppiumLibrary
    Open Application    http://127.0.0.1:4723/wd/hub    platformName=Android    platformVersion=11.0    deviceName=emulator-5554    appPackage=com.google.android.apps.maps    appActivity=com.google.android.maps.driveabout.app.DestinationActivity    noReset=true

Search
    [Arguments]    ${inputFrom}    ${inputTo}    ${HourOutpuut}    ${KilometerOutput}
    Wait Until Page Contains Element    xpath=//android.widget.FrameLayout[contains(@content-desc,"Directions")]
    Click Element    xpath=//android.widget.FrameLayout[contains(@content-desc,"Directions")]
    sleep    1
    Click Element    id=com.google.android.apps.maps:id/directions_startpoint_textbox
    Wait Until Page Contains Element    id=com.google.android.apps.maps:id/search_omnibox_edit_text
    Input Text     id=com.google.android.apps.maps:id/search_omnibox_edit_text    ${inputFrom}
    Press Keycode    66
    Click Element    id=com.google.android.apps.maps:id/directions_endpoint_textbox
    Wait Until Page Contains Element    id=com.google.android.apps.maps:id/search_omnibox_edit_text
    Input Text     id=com.google.android.apps.maps:id/search_omnibox_edit_text    ${inputTo}
    Press Keycode    66
    sleep    5s
    ${CheckHr}    Get Text    xpath=//android.widget.FrameLayout[@content-desc]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]
    ${CheckKm}    Get Text    xpath=//android.widget.FrameLayout[@content-desc]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[2]
    ${CheckKm}    Remove String    ${CheckKm}    (    )
    Append To List    ${HourOutpuut}    ${CheckHr}
    Append To List    ${KilometerOutput}    ${CheckKm}
    Log To Console    ${EMPTY}
    Log To Console    Stats --> PASS
    Close All Applications

Open Chrome
    Set Library Search Order    AppiumLibrary
    Open Application    http://127.0.0.1:4723/wd/hub    platformName=Android    platformVersion=11.0    deviceName=emulator-5554    appPackage=com.android.chrome    appActivity=com.google.android.apps.chrome.Main    noReset=true

Search Word
    [Arguments]    ${word}    ${Company}    ${Address}    ${Telephone}    ${Email}
    sleep    2s
    Click Element    id=com.android.chrome:id/search_box_text
    sleep    1s
    Input Text    id=com.android.chrome:id/url_bar    ${word}
    sleep    2s
    Press Keycode    66
    Wait Until Page Contains Element    //android.view.View[contains(@content-desc,"Extosoft https://extosoft.com › about-us About us")]
    Click Element    //android.view.View[contains(@content-desc,"Extosoft https://extosoft.com › about-us About us")]
    sleep    7s
    Swipe By Percent    50    100    50    0
    sleep    3s
    ${NameCompany}    Get Text    xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View/android.view.View/android.view.View[3]/android.view.View/android.view.View[1]/android.view.View[2]
    ${NameCompany}    Remove String    ${NameCompany}    52/12 soi Ladprao 101 soi 46 (kittichit), Ladprao rd., Khlong Chan, Bang Kapi, Bangkok 10240
    ${AddressCompany}    Get Text    xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View/android.view.View/android.view.View[3]/android.view.View/android.view.View[1]/android.view.View[2]
    ${AddressCompany}    Remove String    ${AddressCompany}    Extosoft Co.,Ltd.
    ${TelephoneCompany}    Get Text    xpath=//android.view.View[@content-desc="025506271"]/android.widget.TextView[2]
    ${EmailCompany}    Get Text    xpath=//android.view.View[@content-desc="support@extosoft.com"]/android.widget.TextView[2]
    Append To List    ${Company}    ${NameCompany}
    Append To List    ${Address}    ${AddressCompany}
    Append To List    ${Telephone}    ${TelephoneCompany}
    Append To List    ${Email}    ${EmailCompany}
    Close All Browsers
