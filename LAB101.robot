*** Settings ***
Library           SeleniumLibrary
Library           DateTime
Library           String
Library           Collections
Library           OperatingSystem
Library           BuiltIn
Library           Dialogs
Library           RequestsLibrary
Library           JSONLibrary
Library           XML
Library           ExcelRobot
Library           Dialogs

*** Test Cases ***
Change City
    Set Library Search Order    ExcelRobot
    Open Excel    D:\\Robot Training\\LAB_Web\\TestChangeCity.xlsx
    ${columnCountInput}    Get Column Count    INPUT
    ${rowCountInput}    Get Row Count    INPUT
    ${columnCountOutput}    Get Column Count    OUTPUT
    @{resultUsernameList}    Create List
    @{resultPasswordList}    Create List
    @{resultCityList}    Create List
    @{userList}    Create List
    @{cityList}    Create List
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${username}    Read Cell Data    INPUT    0    ${input}
        ${password}    Read Cell Data    INPUT    1    ${input}
        ${city}    Read Cell Data    INPUT    2    ${input}
        Append To List    ${resultUsernameList}    ${username}
        Append To List    ${resultPasswordList}    ${password}
        Append To List    ${resultCityList}    ${city}
    END
    ${LengthUser}    Get Length    ${resultUsernameList}
    FOR    ${j}    IN RANGE    0    ${LengthUser}
        ${inputUser}    Set Variable    ${resultUsernameList}[${j}]
        ${inputPwd}    Set Variable    ${resultPasswordList}[${j}]
        ${inputCity}    Set Variable    ${resultCityList}[${j}]
        Open Web
        Log In    ${inputUser}    ${inputPwd}    ${userList}
        Go My Account
        Edit City    ${inputCity}    ${cityList}
        Sign Out
    END
    ${rowCountOutput}    Get Length    ${userList}
    Open Excel To Write    D:\\Robot Training\\LAB_Web\\TestChangeCity.xlsx    new_path=D:\\Robot Training\\LAB_Web\\TestChangeCityCopy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${rowCountOutput}
        ${i}    Evaluate    ${output}+2
        Write To Cell By Name    OUTPUT    A${i}    ${userList}[${output}]
        Write To Cell By Name    OUTPUT    B${i}    ${cityList}[${output}]
    END
    Save Excel

TC_LAB
    Open Web Testit    https://www.testit.co.th/    chrome

How to use Evaluate
    ${result1}    Evaluate    1 + 2
    ${result2}    Evaluate    10 - 5
    ${result3}    Evaluate    ${result1} + ${result2}
    ${result4}    Evaluate    ${result2} * ${result3}
    ${result5}    Evaluate    ${result2} / ${result3}
    ${result6}    Evaluate    random.randint(100,200)
    ### Result ###
    Log To Console    ${result1} = 1 + 2
    Log To Console    ${result2} = 10 - 5
    Log To Console    ${result3} = \ ${result1} + ${result2}
    Log To Console    ${result4} = \ ${result2} * ${result3}
    Log To Console    ${result5} = \ ${result2} / ${result3}
    Log To Console    ${result6}

How to use Convert To
    ${result1}    Convert To Integer    200
    ${result2}    Convert To Binary    210
    ${result3}    Convert To Number    42.1343434    2
    ${result4}    Convert To String    12 String
    ${result5}    Convert To Hex    10
    ### Result ###
    Log To Console    Convert To Integer -> ${result1}
    Log To Console    Convert To Binary -> ${result2}
    Log To Console    Convert To Number -> ${result3}
    Log To Console    Convert To String -> ${result4}
    Log To Console    Convert To Hex -> ${result5}

How to use Should Be Equal
    Should Be Equal    1 THB    1 THB
    Should Be Equal As Numbers    2.5    2.5
    Should Be Equal As Integers    ABCD    abcd    base=16
    Should Be Equal As Integers    10    10
    Should Be Equal As Strings    John    John

How to use Strings
    ${valueStr1}    Catenate    Hello World!!!
    ${valueStr2}    Remove String    Robot Framework    ${SPACE}    Frame
    ${valueStr3}    Split String    Hello World!!!    ${SPACE}
    ${valueStr4}    Replace String    Hello World!!!    ${SPACE}    +

How to use Collections List
    Log To Console    XXXXXX
    @{List}    Create List    B    A    C    B    3
    Append To List    ${List}    D
    Append To List    ${List}    Z
    Log To Console    ${List}
    ${countValue}    Count Values In List    ${List}    Z
    Insert Into List    ${List}    0    C
    Log To Console    ${List}
    Insert Into List    ${List}    3    A
    Log To Console    ${List}
    Sort List    ${List}
    Log To Console    ${List}
    Remove Values From List    ${List}    A
    Log To Console    ${List}

How to use Collections Dictionary
    Log To Console    xxxxx
    &{animals}    Create Dictionary    Dog=4    Spider=8    Human=2
    ${valueLeg}    Get From Dictionary    ${animals}    Human
    Remove From Dictionary    ${animals}    Human
    Log To Console    ${animals}
    Set To Dictionary    ${animals}    Human=2
    Log To Console    ${animals}
    Set To Dictionary    ${animals}    Monkey=2
    Log To Console    ${animals}
    Log Dictionary    ${animals}
    &{headers}    Create Dictionary    Content-Type=applcation/json    Token= Bearer xxxxxx

How to use Capture Screen
    Open Web Mendix
    Capture Page Screenshot    D:\\Robot Training\\screen01.png
    Close Browser

How to use For Loop
    @{animalList}    Create List    Human    Dog    Cat    Spider
    Log To Console    ${EMPTY}
    FOR    ${i}    IN RANGE    0    4
        Log To Console    Loop1 Animal --> ${animalList}[${i}]
    END
    FOR    ${i}    IN    1
        Log To Console    Loop2 Animal --> ${animalList}[${i}]
    END

How to use IF Condition
    ${point}    Set Variable    50
    ${grade}    Set Variable If    ${point} >= 80    A    B
    Set Suite Variable    ${Message1}    A
    Show Grade

How to use Run Keyword If
    ${point}    Set Variable    65
    Log To Console    Point = ${point}
    Run Keyword If    ${point} >=80    Log To Console    Grade A
    ...    ELSE IF    80 > ${point} >= 70    Log To Console    Grade B
    ...    ELSE IF    70 > ${point} >= 60    Log To Console    Grade C
    ...    ELSE    Log To Console    Grade D

How to use Excel
    Set Library Search Order    ExcelRobot
    Open Excel    D:\\Robot Training\\LAB_Web\\TestData1.xlsx
    ${columnCountInput}    Get Column Count    INPUT
    ${rowCountInput}    Get Row Count    INPUT
    ${columnCountOutput}    Get Column Count    OUTPUT
    @{resultUsernameList}    Create List    ${EMPTY}
    @{resultPasswordList}    Create List    ${EMPTY}
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${tag}    Read Cell Data    INPUT    0    ${input}
        Run Keyword If    '${tag}' != 'Run'    Continue For Loop
        ${username}    Read Cell Data    INPUT    1    ${input}
        ${password}    Read Cell Data    INPUT    2    ${input}
        Append To List    ${resultUsernameList}    ${username}
        Append To List    ${resultPasswordList}    ${password}
    END
    Remove From List    ${resultUsernameList}    0
    Remove From List    ${resultPasswordList}    0
    ${rowCountOutput}    Get Length    ${resultUsernameList}
    Open Excel To Write    D:\\Robot Training\\LAB_Web\\TestData1.xlsx    new_path=D:\\Robot Training\\LAB_Web\\TestData1Copy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${rowCountOutput}
        ${i}    Evaluate    ${output}+2
        Write To Cell By Name    OUTPUT    A${i}    PASS    data_type=TEXT
        Write To Cell By Name    OUTPUT    B${i}    SUCCESS
        Write To Cell By Name    OUTPUT    C${i}    ${resultUsernameList}[${output}]
        Write To Cell By Name    OUTPUT    D${i}    ${resultPasswordList}[${output}]
    END
    Save Excel

ATM
    Log to Console    --------------------------
    ${balance}    Evaluate    9988
    Log to Console    I have ${balance} Baht
    ${mod1000}    Evaluate    ${balance}%1000
    ${balance-mod1000}    Evaluate    ${balance}-${mod1000}
    ${lastbalancefor1000}    Evaluate    ${balance-mod1000}/1000
    ${convert1000}    Convert To Integer    ${lastbalancefor1000}
    Run Keyword If    ${lastbalancefor1000} >= 1    Log to Console    I Get Bank 1000: ${convert1000} \ Bank
    ${mod500}    Evaluate    ${mod1000}%500
    ${mod1000-mod500}    Evaluate    ${mod1000}-${mod500}
    ${lastbalancefor500}    Evaluate    ${mod1000-mod500}/500
    ${convert500}    Convert To Integer    ${lastbalancefor500}
    Run Keyword If    ${lastbalancefor500} >= 1    Log to Console    I Get Bank 500: ${convert500} Bank
    ${mod100}    Evaluate    ${mod500}%100
    ${mod500-mod100}    Evaluate    ${mod500}-${mod100}
    ${lastbalancefor100}    Evaluate    ${mod500-mod100}/100
    ${convert100}    Convert To Integer    ${lastbalancefor100}
    Run Keyword If    ${lastbalancefor100} >= 1    Log to Console    I Get Bank 100: ${convert100} Bank
    ${mod50}    Evaluate    ${mod100}%50
    ${mod100-mod50}    Evaluate    ${mod100}-${mod50}
    ${lastbalancefor50}    Evaluate    ${mod100-mod50}/50
    ${convert50}    Convert To Integer    ${lastbalancefor50}
    Run Keyword If    ${lastbalancefor50} >= 1    Log to Console    I Get Bank 50: ${convert50} Bank
    ${mod20}    Evaluate    ${mod50}%20
    ${mod50-mod20}    Evaluate    ${mod50}-${mod20}
    ${lastbalancefor20}    Evaluate    ${mod50-mod20}/20
    ${convert20}    Convert To Integer    ${lastbalancefor20}
    Run Keyword If    ${lastbalancefor20} >= 1    Log to Console    I Get Bank 20: ${convert20} Bank
    ${mod10}    Evaluate    ${mod20}%10
    ${mod20-mod10}    Evaluate    ${mod20}-${mod10}
    ${lastbalancefor10}    Evaluate    ${mod20-mod10}/10
    ${convert10}    Convert To Integer    ${lastbalancefor10}
    Run Keyword If    ${lastbalancefor10} >= 1    Log to Console    I Get Coin 10: ${convert10} Coin
    ${mod5}    Evaluate    ${mod10}%5
    ${mod10-mod5}    Evaluate    ${mod10}-${mod5}
    ${lastbalancefor5}    Evaluate    ${mod10-mod5}/5
    ${convert5}    Convert To Integer    ${lastbalancefor5}
    Run Keyword If    ${lastbalancefor5} >= 1    Log to Console    I Get Coin 5: ${convert5} Coin
    ${mod2}    Evaluate    ${mod5}%2
    ${mod5-mod2}    Evaluate    ${mod5}-${mod2}
    ${lastbalancefor2}    Evaluate    ${mod5-mod2}/2
    ${convert2}    Convert To Integer    ${lastbalancefor2}
    Run Keyword If    ${lastbalancefor2} >= 1    Log to Console    I Get Coin 2: ${convert2} Coin
    ${mod1}    Evaluate    ${mod2}%1
    ${mod2-mod1}    Evaluate    ${mod2}-${mod1}
    ${lastbalancefor1}    Evaluate    ${mod2-mod1}/1
    ${convert1}    Convert To Integer    ${lastbalancefor1}
    Run Keyword If    ${lastbalancefor1} >= 1    Log to Console    I Get Coin 1: ${convert1} Coin

Quiz Test 01
    [Documentation]    Quiz Test 01
    ...    Programming Test
    ...    If writing a Logic Summary Numbers in this order [1, -3, -5, 0, 4, 5]
    ...    Lets calculate the percentage of the count.
    ...
    ...    ทดสอบการเขียนโปรแกรม
    ...    ถ้าให้เขียน Logic Summary
    ...    จำนวนตามลำดับนี้ [1, -3, -5, 0, 4, 5]
    ...    ให้คำนวณเปอร์เช็นของจำนวนนับออกมา
    Log To Console    ${EMPTY}
    @{List}    Create List    1    -3    -5    0    4    5    15
    @{NewList}    Create List
    ${valueLength}    Get Length    ${List}
    Log To Console    ${List}
    FOR    ${i}    IN RANGE    ${valueLength}
        Log To Console    ${List[${i}]}
        Run Keyword If    ${List[${i}]} >= 1    Append To List    ${NewList}    ${List[${i}]}
    END
    ${CountList}    Get Length    ${NewList}
    ${Calculate}    Evaluate    ${CountList}/ ${valueLength} * 100
    ${Calculatesucess}    Convert To Number    ${Calculate}    2
    Log To Console    Percentage = ${Calculatesucess} %

Quiz Test 01v1
    [Documentation]    Quiz Test 01
    ...    Programming Test
    ...    If writing a Logic Summary Numbers in this order [1, -3, -5, 0, 4, 5]
    ...    Lets calculate the percentage of the count.
    ...
    ...    ทดสอบการเขียนโปรแกรม
    ...    ถ้าให้เขียน Logic Summary
    ...    จำนวนตามลำดับนี้ [1, -3, -5, 0, 4, 5]
    ...    ให้คำนวณเปอร์เช็นของจำนวนนับออกมา
    Log To Console    ${EMPTY}
    @{List}    Create List    1    -3    -5    0    4    5    20
    ${valueLeg}    Get Length    ${List}
    Log To Console    ${List}
    ${count}    Set Variable    0
    FOR    ${i}    IN RANGE    ${valueLeg}
        Log To Console    ${List[${i}]}
        Run Keyword If    ${List[${i}]}<= 0    Continue For Loop
        ${count}    Evaluate    ${count}+1
    END
    ${Calculate}    Evaluate    ${count}/${valueLeg} *100
    ${Calculatesucess}    Convert To Number    ${Calculate}    2
    Log To Console    Percentage = ${Calculatesucess} %

Quiz Test 02
    ${value}    Get Value From User    Please Input Value :
    Log To Console    ${EMPTY}
    @{List}    CreateList
    FOR    ${i}    IN RANGE    ${value}
        TextXO    ${i}    ${value}    ${List}
    END

Quiz Test 03
    [Documentation]    Quiz Test 03
    ...    รับค่า 1 < X <= 1000
    ...    จงหาจำนวนเฉพาะทั้งหมด ต้้งแต่ 1 ถึง X ว่ามีกี่เลข อะไรบ้าง ตัวอย่างดังนี้
    ...
    ...    TC01; X=10
    ...    Total prime number = \ 4
    ...    Prime number = 2, 3, 5, 7
    ...
    ...    TC02; X=50
    ...    Total prime number = 15
    ...    Prime number = \ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47
    Log To Console    ${EMPTY}
    ${value}    Get Value From User    Please Input Value (1<X<1000) :
    Run Keyword If    ${value} <= 1 or ${value} > 1000    Log To Console    Please Input Value (1<X<1000)
    @{ListPrime}    Create List
    FOR    ${i}    IN RANGE    2    ${value}+1
        CalculatePrime    ${i}    ${ListPrime}
    END
    ${AllPrime}    BuiltIn.Get Length    ${ListPrime}
    Log To Console    Total prime number : ${AllPrime}
    Log To Console    Prime Number : ${ListPrime}

3 Cups Montes
    Log To Console    ${EMPTY}
    ${value}    Get Value From User    Please input value A, B, C:
    Log To Console    Input Is: ${value}
    ${valueResult}    Remove String    ${value}    AA    BB    CC
    &{cups}    Create Dictionary    Cup1=1    Cup2=0    Cup3=0
    ${valuelength}    Get Length    ${valueResult}
    FOR    ${index}    IN RANGE    0    ${valuelength}
        ${Cup1Value}    Get From Dictionary    ${cups}    Cup1
        ${Cup2Value}    Get From Dictionary    ${cups}    Cup2
        ${Cup3Value}    Get From Dictionary    ${cups}    Cup3
        Run Keyword If    '${valueResult}[${index}]'=='A'    Set To Dictionary    ${cups}    Cup1=${cup2value}    Cup2=${cup1value}
        Run Keyword If    '${valueResult}[${index}]'=='B'    Set To Dictionary    ${cups}    Cup2=${cup3value}    Cup3=${cup2value}
        Run Keyword If    '${valueResult}[${index}]'=='C'    Set To Dictionary    ${cups}    Cup1=${cup3value}    Cup3=${cup1value}
    END
    ${Cup1Value}    Get From Dictionary    ${cups}    Cup1
    ${Cup2Value}    Get From Dictionary    ${cups}    Cup2
    ${Cup3Value}    Get From Dictionary    ${cups}    Cup3
    Log Dictionary    ${cups}
    Run Keyword If    ${Cup1Value}==1    Log To Console    Output Is: Cup 1
    ...    ELSE IF    ${Cup2Value}==1    Log To Console    Output Is: Cup 2
    ...    ELSE IF    ${Cup3Value}==1    Log To Console    Output Is: Cup 3

3 Cups Monte
    [Documentation]    แก้วที่ 1 2 3 บอลเริ่มต้น อยู่ในแก้วที่ 1 เสมอ
    ...
    ...    A คือ สลับแก้วที่ 1 และ 2
    ...    B คือ สลับแก้วที่ 2 และ 3
    ...    C คือ สลับแก้วที่ 1 และ 3
    ...
    ...    จงหาว่าบอลอยู่แก้วไหน
    ...
    ...    Input: AB
    ...    Output: 3
    ...
    ...    Input: ABC
    ...    Output: 1
    ...
    ...    Input: CBABCACCC
    ...    Output: 1
    Log To Console    ${EMPTY}
    ${Input}    Get Value From User    Please input A | B | C :
    @{List}    Convert To List    ${Input}
    ${Pointer}    Set Variable    1
    ${LengthList}    Get Length    ${List}
    FOR    ${i}    IN RANGE    ${LengthList}
        Run Keyword If    '${List}[${i}]' == 'A' \ and ${Pointer} == 1    Cup 1 To 2    ${Pointer}
        ...    ELSE IF    '${List}[${i}]' == 'A' \ and ${Pointer} == 2    Cup 2 To 1    ${Pointer}
        Run Keyword If    '${List}[${i}]' == 'B' \ and ${Pointer} == 2    Cup 2 To 3    ${Pointer}
        ...    ELSE IF    '${List}[${i}]' == 'B' \ and ${Pointer} == 3    Cup 3 To 2    ${Pointer}
        Run Keyword If    '${List}[${i}]' == 'C' \ and ${Pointer} == 3    Cup 3 To 1    ${Pointer}
        ...    ELSE IF    '${List}[${i}]' == 'C' \ and ${Pointer} == 1    Cup 1 To 3    ${Pointer}
    END
    Log To Console    Input : ${Input}
    Log To Console    Output : ${Pointer}

Get Text Web Robot
    [Documentation]    เงื่อนไข
    ...    - ดึงข้อมูลจาก excel มาทำการกรอกข้อมูลในหน้าเว็บ
    ...    - สามารถรันได้มากกว่า 1 รอบ หรือสามารถ เลือกได้ว่าจะสั่ง Run ที่ data ไหนได้บ้าง
    ...    - เก็บข้อมูลจากหน้าจอลง sheet OUTPUT
    ...
    ...    4.http://advantageonlineshopping.com (สมัคร uer ก่อน)
    ...
    ...    (ของพี่) User : xxx
    ...    Pass : xxx
    ...
    ...    TC01_01_OpenWeb
    ...    TC01_02_Login
    ...    TC01_03_ClickMyAccount
    ...    TC01_04_ClickEditAccountDetails
    ...    TC01_05_InputCityAndClickSave
    ...    TC01_06_ClickLogout
    Open Web RobotFrameWork

Shop I Pad
    Set Library Search Order    ExcelRobot
    Open Excel    D:\\Robot Training\\LAB_Web\\TestShopIpad.xlsx
    ${columnCountInput}    Get Column Count    INPUT
    ${rowCountInput}    Get Row Count    INPUT
    ${columnCountOutput}    Get Column Count    OUTPUT
    @{resultUsernameList}    Create List
    @{resultPasswordList}    Create List
    @{resultProductName}    Create List
    @{resultQuanTity}    Create List
    @{resultPrice}    Create List
    @{userList}    Create List
    @{productName}    Create List
    @{quanTity}    Create List
    @{price}    Create List
    @{ListQuantity}    Create List
    FOR    ${input}    IN RANGE    1    ${rowCountInput}
        ${test}    Read Cell Data    INPUT    0    ${input}
        Run Keyword if    '${test}' != 'Run'    Continue For Loop
        ${username}    Read Cell Data    INPUT    1    ${input}
        ${password}    Read Cell Data    INPUT    2    ${input}
        ${quantityamount}    Read Cell Data    INPUT    3    ${input}
        Append To List    ${resultUsernameList}    ${username}
        Append To List    ${resultPasswordList}    ${password}
        Append To List    ${ListQuantity}    ${quantityamount}
    END
    ${LengthUser}    Get Length    ${resultUsernameList}
    FOR    ${j}    IN RANGE    0    ${LengthUser}
        ${inputUser}    Set Variable    ${resultUsernameList}[${j}]
        ${inputPwd}    Set Variable    ${resultPasswordList}[${j}]
        ${inputQutity}    Set Variable    ${ListQuantity}[${j}]
        Open Web
        Log In    ${inputUser}    ${inputPwd}    ${userList}
        Search Tablets    ${inputQutity}
        Remove I Pad    ${productName}    ${quanTity}    ${price}
        Sign Out
    END
    ${rowCountOutput}    Get Length    ${userList}
    Open Excel To Write    D:\\Robot Training\\LAB_Web\\TestShopIpad.xlsx    new_path=D:\\Robot Training\\LAB_Web\\TestShopIpadCopy.xlsx    override=override
    FOR    ${output}    IN RANGE    0    ${rowCountOutput}
        ${i}    Evaluate    ${output}+2
        Write To Cell By Name    OUTPUT    A${i}    ${ListStatus} \
        Write To Cell By Name    OUTPUT    B${i}    ${userList}[${output}]
        Write To Cell By Name    OUTPUT    C${i}    ${productName}[${output}]
        Write To Cell By Name    OUTPUT    D${i}    ${quanTity}[${output}]
        Write To Cell By Name    OUTPUT    E${i}    ${price}[${output}]
    END
    Save Excel

*** Keywords ***
Search Tablets
    [Arguments]    ${inputQutity}
    Sleep    1s
    Click Element    //*[@id="tabletsImg"]
    Sleep    2s
    Click Element    //*[@id="16"]
    Sleep    2s
    Click Element    //span[@title="GRAY"]
    FOR    ${i}    IN RANGE    1    ${inputQutity}
        Click Element    //*[@class="plus"]
    END
    Click Element    //*[@name="save_to_cart"]
    Click Element    //*[@id="menuCart"]

Open Web
    Open Browser    http://advantageonlineshopping.com/#/    chrome    options=add_experimental_option("detach", True)
    Maximize Browser Window
    Wait Until Page Contains Element    //*[@id="menuUser"]    10s
    Click Element    //*[@id="menuUser"]

Log In
    [Arguments]    ${inputUser}    ${inputPwd}    ${userList}
    Sleep    4s
    Press Keys    //input[@name="username"]    ${inputUser}
    Press Keys    //input[@name="password"]    ${inputPwd}
    Click Element    //button[@id="sign_in_btnundefined"]
    Sleep    2s
    ${CheckUser}    Get Text    //span[@class="hi-user containMiniTitle ng-binding"]
    Append To List    ${userList}    ${CheckUser}
    Log To Console    ${EMPTY}
    Run Keyword If    '${CheckUser}' == '${inputUser}'    Log To Console    ${inputUser} --> LogIn Success
    ...    ELSE    Log To Console    LogIn Fail

Go My Account
    Sleep    1s
    Click Element    //*[@id="menuUser"]
    Sleep    1s
    Click Element    //*[@class="option roboto-medium ng-scope"][text()="My account"]
    Sleep    2s
    ${CheckPageMyaccount}    Get Text    //*[@class="roboto-regular sticky fixedImportant ng-scope"]
    Run Keyword If    '${CheckPageMyaccount}' == 'MY ACCOUNT'    Log To Console    Go Page Account Success
    ...    ELSE    Log To Console    Go Page Account Fail

Edit City
    [Arguments]    ${inputCity}    ${cityList}
    Sleep    2s
    Click Element    //a[@class="floatRigth ng-scope"]
    Sleep    3s
    Input Text    //input[@name="cityAccountDetails"]    ${inputCity}
    Click Element    //button[@id="save_btnundefined"]
    Sleep    3s
    ${CheckCity}    Get Text    //label[@data-ng-hide="accountDetails.cityName == ''"][1]
    Append To List    ${cityList}    ${CheckCity}
    Run Keyword If    '${CheckCity}' == '${inputCity}'    Log To Console    Edit Success
    ...    ELSE    Log To Console    Edit Fail

Sign Out
    Sleep    1s
    Click Element    //*[@id="menuUser"]
    Sleep    1s
    Click Element    //*[@class="option roboto-medium ng-scope"][text()="Sign out"]
    Sleep    2s
    Close Browser

Open Web Testit
    [Arguments]    ${URL}    ${Browser}
    Open Browser    ${URL}    ${Browser}    options=add_experimental_option("detach", True)
    Maximize Browser Window

Open Web Mendix
    Open Browser    https://www.mendix.com    chrome    options=add_experimental_option("detach", True)
    Maximize Browser Window
    Wait Until Page Contains Element    //span[contains(.,'Start for free')]    10s
    ${text}=    Get Text    //span[contains(.,'Start for free')]
    Run Keyword If    '${text}' == 'Start for free'    Log To Console    Open Web Mendix Success
    ...    ELSE    Log To Console    Open Web Mendix Fail

Show Grade
    Log To Console    ${Message1}

countAll
    [Arguments]    ${count}
    ${countAll}    Evaluate    ${count}+1

TextXO
    [Arguments]    ${i}    ${value}    ${List}
    FOR    ${j}    IN RANGE    ${value}
        Run Keyword If    ${j} == ${i}    Append To List    ${List}    X
        Append To List    ${List}    O
    END
    Remove From List    ${List}    ${value}
    Log To Console    ${List}
    Remove Values From List    ${List}    X    O

CalculatePrime
    [Arguments]    ${i}    ${ListPrime}
    ${Count}    Set Variable    0
    FOR    ${j}    IN RANGE    2    ${i}
        Run Keyword If    ${Count} > 0    Exit For Loop
        Run Keyword If    ${i}%${j} != 0    Continue For Loop
        ${Count}    Evaluate    ${Count} + 1
    END
    Run Keyword If    ${Count} == 0    Append To List    ${ListPrime}    ${i}

Cup 1 To 2
    [Arguments]    ${Pointer}
    Set Suite Variable    ${Pointer}    2

Cup 2 To 1
    [Arguments]    ${Pointer}
    Set Suite Variable    ${Pointer}    1

Cup 2 To 3
    [Arguments]    ${Pointer}
    Set Suite Variable    ${Pointer}    3

Cup 3 To 2
    [Arguments]    ${Pointer}
    Set Suite Variable    ${Pointer}    2

Cup 1 To 3
    [Arguments]    ${Pointer}
    Set Suite Variable    ${Pointer}    3

Cup 3 To 1
    [Arguments]    ${Pointer}
    Set Suite Variable    ${Pointer}    1

Open Web RobotFrameWork
    Open Browser    https://robotframework.org/    chrome    options=add_experimental_option("detach", True)
    Maximize Browser Window
    ${text}    Get Text    //*[@style="top: 247px; height: 19px;"]//span[@class="mtk1"]
    ${LastText}    Remove String    ${text}    ${SPACE}
    Log To Console    ${EMPTY}
    Log To Console    ${LastText}
    Close Browser

Remove I Pad
    [Arguments]    ${productName}    ${quanTity}    ${price}
    Sleep    3s
    ${CheckProductName}    Get Text    //*[@class="roboto-regular productName ng-binding"]
    ${CheckProductQuantity}    Get Text    //*[@class="smollCell quantityMobile"]//*[@class="ng-binding"]
    ${CheckProductPrice}    Get Text    //*[@class='smollCell']//*[@class='price roboto-regular ng-binding']
    ${CheckProductPrice}    Remove String    ${CheckProductPrice}    $
    Append To List    ${productName}    ${CheckProductName}
    Append To List    ${quanTity}    ${CheckProductQuantity}
    Append To List    ${price}    ${CheckProductPrice}
    Sleep    3s
    Click Element    //*[@id="shoppingCart"]/table/tbody/tr[1]/td[6]/span/a[3]
    Sleep    3s
    ${CheckStatus}    Get Text    //*[@id="shoppingCart"]/div/label
    Run Keyword If    '${CheckStatus}' == 'Your shopping cart is empty'    Set Suite Variable    ${ListStatus}    PASS
    ...    ELSE    Set Suite Variable    ${ListStatus}    FAIL
    Run Keyword If    '${ListStatus}' == 'PASS'    Log To Console    Status --> ${ListStatus}
    ...    ELSE    Log To Console    Status --> ${ListStatus}
